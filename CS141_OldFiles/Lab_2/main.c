#include <stdio.h>

void main() {

	/* Stage 0 */
	printf("*\n");

	/* Stage 1 */
	int x;
	printf("Please enter a number: ");
	scanf("%d", &x);
	printf("\n");

	for(int i = 0; i < x; i++) {
		printf("* ");
	}
	printf("\n\n");

	/* Stage 2 */
	for(int i = 0; i < x; i++) {
		for(int k = 0; k < x; k++) {
			printf("*");
		}
		printf("\n");
	}

	printf("\n");

	/* Stage 3 */
	for(int i = 0; i < x; i++) {
		for(int k = 0; k <= i; k++) {
			printf("*");
		}
		printf("\n");
	}
	printf("\n");
	getchar();
}
