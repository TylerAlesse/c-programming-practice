#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct node {
    int x;
    struct node *next;
};

void main() {
    struct node *head; // head node, here to not lose it in memory
    struct node *tracker; // tracker node

    // head points to a node struct
    head = (struct node *) malloc(sizeof(struct node));

    // this works the same as the above
    // head = malloc(sizeof(struct node));

    // Assigning values in head node
    head->x = 1;
    head->next = NULL;

    // Assign head node to tracker node
    tracker = head;

    // How the for loop below works
        // struct node *item = (struct node *) malloc(sizeof(struct node));
        // item->x = 2;
        // item->next = head;
        // tracker = item;
        // item = (struct node *) malloc(sizeof(struct node));
        // item->x = 3;
        // item->next = tracker;
        // tracker = item;
        // item = (struct node *) malloc(sizeof(struct node));

    // Creating a stack
    struct node *item = (struct node *) malloc(sizeof(struct node));
    for(int i = 2; i < 4; i++) {
        item->x = i;
        item->next = tracker;
        tracker = item;
        item = (struct node *) malloc(sizeof(struct node));
    }

    // This method of reading tracker makes us lose each
        // item on the stack

    // if tracker doesn't point to null
    while(tracker != NULL) {
        printf("x: %d\n", tracker->x); // print item value
        tracker = tracker->next; // set cond to next in list
    }
}