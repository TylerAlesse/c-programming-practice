#include <stdio.h>
void main() {
    int x;
    int y;
    printf("What is X? ");
    scanf("%d", &x);
    printf("What is Y? ");
    scanf("%d", &y);
    int product = x * y;
    printf("%d * %d = %d\n", x, y, product);
}