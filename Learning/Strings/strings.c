#include <stdio.h>
#include <string.h> // String manip. header

void main() {
    char myString[256];
    
    printf("Please enter a string with less than 256 characters.\n");
    
    // Because arrays are "already pointers"
        // you don't need the & before handing
        // myString to fgets
    fgets(myString, 256, stdin);
    printf("\nYou typed:\n%s", myString);

    // String Comparison
    // Equal: 0
    printf("\n%d\n", strcmp("test", "test"));
    // s1 > s2: 1
    printf("\n%d\n", strcmp("no", "na"));
    // s1 < s2: -1
    printf("\n%d\n", strcmp("na", "no"));
    
    // String Concat
    char foo[16] = "concat";
    printf("\n%s\n", strcat(foo, "enating"));

    // String Copy
    // strcpy(char *dest, const char *src);

    // String Length
    printf("Length of input string: %ld\n", strlen(myString));
}