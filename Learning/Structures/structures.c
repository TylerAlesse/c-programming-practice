#include <stdio.h>

struct employee_data {
    int id;
    int age;
};

// Forward Function Declaration
struct employee_data get_employee();

void main() {
    // struct struct_name variable_name
    struct employee_data employee_a;
    employee_a.id = 1;
    employee_a.age = 22;

    struct employee_data employee_b = get_employee();

    // Pointing to data in a struct
    struct employee_data *ptr;
    ptr = &employee_a;
    printf("%d\n", ptr->age);
}

// structs can also be used as the output variable of a function.
struct employee_data get_employee() {
    struct employee_data employee;

    // Imagine we are obtaining the information
        // from an actual database.
    employee.id = 2;
    employee.age = 30;

    return employee;
}