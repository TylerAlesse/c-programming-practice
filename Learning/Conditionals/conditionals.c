#include <stdio.h>
void main() {
    int user_age;
    printf("How old are you? ");
    scanf("%d", &user_age);
    if(user_age >= 21) {
        printf("Come out to the bar sometime!\n");
    } else {
        printf("You're not old enough to drink!\n");
    }
}