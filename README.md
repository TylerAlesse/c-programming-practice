# README #

### What is this repository for? ###

This repository is intended to archive my practice with the C programming language.

### Who do I talk to? ###

If you would like to contact me, you can find email me at [TylerAlesse@mail.com](mailto:TylerAlesse@mail.com) or [TylerAlesse@gmail.com](mailto:TylerAlesse@gmail.com).