#include <stdio.h>
void main() {
    // Array Example 1
    int scores[10];
    scores[0] = 1;
    printf("%d\n", scores[1]);

    // Array example with user input
    char astring[10];
    scanf("%s", astring);
    for (int i = 0; i < sizeof(astring); i++) {
        if(astring[i] == 'a') {
            printf("You entered an a.\n");
        }
    }

    // Multi-dimensional array example
    int emptyBoard[8][8]; // 2D
    int emptyCube[8][8][8]; // 3D

    // Obtaining the rows and columns of a 2D array

    // sizeof() returns long unsigned int
    printf("Rows: %ld\n", sizeof(emptyBoard)    / sizeof(emptyBoard[0]));
    printf("Cols: %ld\n", sizeof(emptyBoard[0]) / sizeof(int)          );
}