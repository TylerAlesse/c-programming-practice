#include <stdio.h>
int mult (int x, int y);

void main() {
	int x;
	int y;

	printf("Please provide an x and a y value to multiple:\n");
	printf("x: ");
	scanf("%d", &x);
	printf("y: ");
	scanf("%d", &y);
	printf("The product is: %d.\n", mult(x, y));
}

int mult (int x, int y) {
	return x * y;
}
