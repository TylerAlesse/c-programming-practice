#include <stdio.h>

void main(int argc, char *argv[]) {
    if (argc != 2) { // argc must be 2 for correct exec
        printf("usage: %s filename\n", argv[0]);
    } else {
        FILE *file = fopen(argv[1], "r");
        if(file == 0) {
            printf("Could not open file.\n");
        } else {
            int x;
            while( (x = fgetc(file)) != EOF) {
                printf("%c", x);
            }

            printf("\n");
            fclose(file);
        }
    }
}