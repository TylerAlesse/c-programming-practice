#include <stdio.h> /* Include the standard I/O header file when compiling */

/* The main() function is called upon running */
void main() /* void specifies that nothing is returned */
{ /* Begin function */
	printf("Hello, World!"); /* Output "Hello, World!" to the console */
	getchar(); /* Get a user inputted character; submit with [Enter] */
} /* End function */
