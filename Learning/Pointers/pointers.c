#include <stdio.h>
void main() {
	int x;
	int *p;

	// Memory allocation is used when we do not know what size
		// the variable is going to be upon compile time.

	// int *p = malloc( sizeof(*p) );

	// When doing the sizeof(*p), it points to an int,
		// and therefore is the same as the below declaration.
	// The above method is better, such that we only have to
		// change the datatype of p if necessary.
	
	// int *p = malloc( sizeof(int) );

	// single variable output example
	p = &x;
	printf("Int: ");
	scanf("%d", &x);
	printf("Value at p: %d\n", *p);
	printf("p address: %p\n", p);

	// Multi-variable output example
	int var;
	printf("Int: ");
	scanf("%d", &var);
	printf("Value: %d; Address: %p\n", var, &var);

	// Must free the memory after allocation.
	// free(*p);

	// It is a good idea to make it a null pointer after freeing.
	// *p = NULL;
}
