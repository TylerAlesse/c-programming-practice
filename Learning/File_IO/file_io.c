#include <stdio.h>
#include <stdlib.h>

void main() {
    FILE *fp;
    fp = fopen("test.txt", "r");

    if(fp == NULL) {
        printf("Could not open file.\n");
        exit(0);
    }

    char chr;
    chr = fgetc(fp);
    while(chr != EOF) {
        printf("%c", (chr));
        chr = fgetc(fp);
    }

    printf("\n");
    fclose(fp);
}