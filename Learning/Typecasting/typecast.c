#include <stdio.h>

void main() {
    // Typecasting the integer to a character
    printf("%c\n", (char)65); // 65 == A

    printf("\n"); // Spacing
    for(int i = 0; i < 128; i++) {
        // Typecasting a bunch of ints to chars
        printf("%d: %c\n", i, i);

        // the %c is the typecast in this case
            // the (char) isn't necessary
    }
}